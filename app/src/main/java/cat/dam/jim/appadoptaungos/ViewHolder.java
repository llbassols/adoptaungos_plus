package cat.dam.jim.appadoptaungos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ViewHolder extends RecyclerView.ViewHolder {

    View mView;

    public ViewHolder(View itemView){
        super(itemView);

        mView = itemView;
    }

    public void setdatails(Context ctx, String nom, String raça, String imatge){
        TextView mNomView = mView.findViewById(R.id.rnom);
        TextView mdescripcio = mView.findViewById(R.id.rdescripcio);
        ImageView mimatge = mView.findViewById(R.id.rimatge);

        mNomView.setText(nom);
        mdescripcio.setText(raça);
        Picasso.get().load(imatge).into(mimatge);
    }
}
