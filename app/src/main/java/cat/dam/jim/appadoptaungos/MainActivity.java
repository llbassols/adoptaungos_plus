package cat.dam.jim.appadoptaungos;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button bt_buscar, bt_posar_adopcio, bt_pref;
    RadioGroup rg_sexe;
    RadioButton rb_tots, rb_femella, rb_mascle;
    AutoCompleteTextView ac_tv;
    TextView tv_km;
    SeekBar skbar;
    LinearLayout linear_layout_menu, linear_layout_search, linear_layout_main_page;
    RelativeLayout rl_preferencies, rl_search;
    ListView lv_animals;

    FirebaseDatabase fbdb;
    DatabaseReference dbrf;

    private List<Animal> llistaAnimals = new ArrayList<Animal>();
    ArrayAdapter<Animal> arrayAdapterAnimals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_buscar = (Button)findViewById(R.id.bt_buscar);
        bt_posar_adopcio = (Button)findViewById(R.id.bt_posar_adopcio);
        bt_pref = (Button)findViewById(R.id.bt_pref);
        rg_sexe = (RadioGroup)findViewById(R.id.rg_sexe);
        rb_femella = (RadioButton)findViewById(R.id.rb_femella);
        rb_mascle = (RadioButton)findViewById(R.id.rb_mascle);
        rb_tots = (RadioButton)findViewById(R.id.rb_tots);
        ac_tv = (AutoCompleteTextView)findViewById(R.id.ac_tv);
        tv_km = (TextView)findViewById(R.id.tv_km);
        skbar = (SeekBar)findViewById(R.id.skbar);
        linear_layout_menu = (LinearLayout)findViewById(R.id.linear_layout_menu);
        rl_preferencies = (RelativeLayout)findViewById(R.id.rl_preferencies);
        rl_search = (RelativeLayout)findViewById(R.id.rl_search);
        lv_animals = (ListView)findViewById(R.id.lv_animals);
        linear_layout_main_page = (LinearLayout)findViewById(R.id.linear_layout_main_page);

        //Iniciar base de dades
        iniciarFirebase();

        llistarAnimals();

        bt_pref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_preferencies.getVisibility() == View.INVISIBLE){
                    rl_preferencies.setVisibility(View.VISIBLE);
                }else{
                    rl_preferencies.setVisibility(View.INVISIBLE);
                }
            }
        });

        bt_posar_adopcio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Main3Activity.class));
            }
        });
    }

    //Metode per iniciar base de dades
    private void iniciarFirebase(){
        FirebaseApp.initializeApp(this);
        fbdb = FirebaseDatabase.getInstance();
        dbrf = fbdb.getReference();
    };

    private void llistarAnimals(){
        dbrf.child("Animal").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                llistaAnimals.clear();
                for (DataSnapshot objSnapchot : dataSnapshot.getChildren()){
                    Animal a = objSnapchot.getValue(Animal.class);
                    llistaAnimals.add(a);

                    arrayAdapterAnimals = new ArrayAdapter<Animal>(MainActivity.this, android.R.layout.simple_list_item_1, llistaAnimals);
                    lv_animals.setAdapter(arrayAdapterAnimals);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
