package cat.dam.jim.appadoptaungos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class VerificarMobil extends AppCompatActivity {

    //objectes que es necessiten
    //codi de verificació que s'enviarà a l'usuari
    private String mVerificationId;

    //editText
    private EditText editTextCode;

    //objecte firebase auth
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificar_mobil);

        //inicialitzar objectes
        mAuth = FirebaseAuth.getInstance();
        editTextCode = findViewById(R.id.editTextCode);


        //agafar el número de telèfon de l'activitat anterior
        //enviant el codi de verificació al número
        Intent intent = getIntent();
        String mobil = intent.getStringExtra("mobil");
        sendVerificationCode(mobil);


        //si la detecció d'SMS automàtic no funciona, l'usuari pot entrar el codi manualment
        findViewById(R.id.btn_continua).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError(getString(R.string.codi_valid));
                    editTextCode.requestFocus();
                    return;
                }
                //verificar el codi entrat manualment
                verifyVerificationCode(code);
            }
        });

    }

    //el mètode envia el codi de verificació
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+34" + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }


    //el callback per detectar l'estat de la verificació
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //agafar el codi que s'ha enviat per SMS
            String code = phoneAuthCredential.getSmsCode();

            //pot ser que el codi no es detecti automaticament
            //en aquest cas el codi serà null
            //per això l'usuari haurà d'entrar manualment el codi
            if (code != null) {
                editTextCode.setText(code);
                //verificar el codi
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
            }
            Toast.makeText(VerificarMobil.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //guardar l'id de verificació que s'ha enviat a l'usuari
            mVerificationId = s;
        }
    };


    private void verifyVerificationCode(String code) {
        //Crear la credencial
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(VerificarMobil.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity
                            startActivity(new Intent(VerificarMobil.this, MainActivity.class));
                        } else {
                            //verification unsuccessful.. display an error message
                            String message = "Somthing is wrong, we will fix it soon...";
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }
                        }
                    }
                });
    }

}