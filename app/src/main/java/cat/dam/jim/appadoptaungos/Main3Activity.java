package cat.dam.jim.appadoptaungos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static cat.dam.jim.appadoptaungos.R.id;
import static cat.dam.jim.appadoptaungos.R.layout;


public class Main3Activity extends AppCompatActivity {

    //NEW
    RecyclerView mRecyclerView;

    Button bt_buscar, bt_posar_adopcio, bt_publicar;
    EditText et_nom, et_tipos, et_raça, et_edat, et_ubi, et_sexe, et_telefon;
    ListView lv_animals_propis;

    FirebaseDatabase fbdb;
    DatabaseReference dbrf;

    private List<Animal> llistaAnimals = new ArrayList<Animal>();
    ArrayAdapter<Animal> arrayAdapterAnimals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //NEW
        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle("animal");
        mRecyclerView = findViewById(id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        fbdb = FirebaseDatabase.getInstance();
        dbrf = fbdb.getReference("Animal");


        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main3);
        bt_buscar = (Button)findViewById(id.bt_buscar);
        bt_posar_adopcio = (Button)findViewById(id.bt_posar_adopcio);
        bt_publicar = (Button)findViewById(id.bt_publicar);
        et_nom = (EditText)findViewById(id.et_nom);
        et_tipos = (EditText)findViewById(id.et_tipos);
        et_raça = (EditText)findViewById(id.et_raça);
        et_edat = (EditText)findViewById(id.et_edat);
        et_ubi = (EditText)findViewById(id.et_ubi);
        et_sexe = (EditText)findViewById(id.et_sexe);
        et_telefon = (EditText)findViewById(id.et_telefon);
        //lv_animals_propis = (ListView)findViewById(R.id.lv_animals_propis);

        //Iniciar base de dades
        iniciarFirebase();


        bt_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Main3Activity.this, MainActivity.class));
            }
        });

        bt_publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobil = et_telefon.getText().toString().trim();

                if(mobil.isEmpty()){
                    et_telefon.setError("Entra un número vàlid");
                    et_telefon.requestFocus();
                    return;
                }

                Intent intent = new Intent(Main3Activity.this, VerificarMobil.class);
                intent.putExtra("mobil", mobil);
                startActivity(intent);

                String nom_animal = et_nom.getText().toString();
                String tipus = et_tipos.getText().toString();
                String raça = et_raça.getText().toString();
                String edat_string= et_edat.getText().toString();
                int edat = Integer.parseInt(edat_string);
                String ciutat = et_ubi.getText().toString();
                String sexe = et_sexe.getText().toString();
                String numero_telf = et_telefon.getText().toString();

                Animal animal = new Animal();
                animal.setId(UUID.randomUUID().toString());
                animal.setNom(nom_animal);
                animal.setTipus(tipus);
                animal.setRaça(raça);
                animal.setAny_naixament(edat);
                animal.setUbicacio(ciutat);
                animal.setSexe(sexe);
                animal.setTelefon_usuari(numero_telf);

                dbrf.child("Animal").child(animal.getId()).setValue(animal);

                et_nom.setText("");
                et_tipos.setText("");
                et_raça.setText("");
                et_edat.setText("");
                et_ubi.setText("");
                et_sexe.setText("");
                et_telefon.setText("");

                Toast.makeText(Main3Activity.this, "Publicat!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void llistarAnimals(){
        dbrf.child("Animal").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                llistaAnimals.clear();
                for (DataSnapshot objSnapchot : dataSnapshot.getChildren()){
                    Animal a = objSnapchot.getValue(Animal.class);
                    llistaAnimals.add(a);

                    arrayAdapterAnimals = new ArrayAdapter<Animal>(Main3Activity.this, android.R.layout.simple_list_item_1, llistaAnimals);
                    lv_animals_propis.setAdapter(arrayAdapterAnimals);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //Metode per iniciar base de dades
    private void iniciarFirebase(){
        FirebaseApp.initializeApp(this);
        fbdb = FirebaseDatabase.getInstance();
        dbrf = fbdb.getReference();
    };
    /*
    private String getPhoneNumber(){
        try{
            TelephonyManager mTelephonyManager;
            mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            return mTelephonyManager.getLine1Number();
        }catch(SecurityException se){
            return "";
        }
    }
    */
    //NEW

    protected void OnCreate(){
        FirebaseRecyclerOptions<Animal> options =
                new FirebaseRecyclerOptions.Builder<Animal>()
                        .setQuery(dbrf, Animal.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<Animal, ViewHolder>(options) {
            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(layout.row, parent, false);

                return new ViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ViewHolder holder, int position, Animal model) {
                holder.setdatails(getApplicationContext(),model.getNom(),model.getRaça(),model.getimatge());
            }
        };
        mRecyclerView.setAdapter(adapter);
    }

}
