package cat.dam.jim.appadoptaungos;

public class Animal {
    private String id;
    private int any_naixament;
    private String nom;
    private String nom_usuari;
    private String raça;
    private String sexe;
    private String telefon_usuari;
    private String tipus;
    private String ubicacio;
    private String imatge;

    public Animal() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAny_naixament() {
        return any_naixament;
    }

    public void setAny_naixament(int any_naixament) {
        this.any_naixament = any_naixament;
    }

    public String getimatge() {
        return imatge;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom_usuari() {
        return nom_usuari;
    }

    public void setNom_usuari(String nom_usuari) {
        this.nom_usuari = nom_usuari;
    }

    public String getRaça() {
        return raça;
    }

    public void setRaça(String raça) {
        this.raça = raça;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getTelefon_usuari() {
        return telefon_usuari;
    }

    public void setTelefon_usuari(String telefon_usuari) {
        this.telefon_usuari = telefon_usuari;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public String getUbicacio() {
        return ubicacio;
    }

    public void setUbicacio(String ubicacio) {
        this.ubicacio = ubicacio;
    }

    @Override
    public String toString() {
        return "Nom: " + nom + "\n" +
                "Any naixament= " + any_naixament + "\n" +
                "Raça: " + raça + "\n" +
                "Sexe: " + sexe + "\n" +
                "Tipus: " + tipus + "\n" +
                "Ubicacio: " + ubicacio + "\n";
    }
}
