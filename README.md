
-Configuracio Git:

    Clonar el projecte:
        Anar a VCS > Git > Clone. i clonar el projecte amb la seva URL

    Definir el remot:
        Al AndroidStudio, anar a VCS > Git > Remotes
        i definir el remot amb la URL del projecte
        
        Un cop definit el remot ja no cal tocar-lo més.
    
    Fer un PULL (Quan vols els canvis del server/Projecte que tu encara no tens)
        Anar a VCS > Git > Pull
        
    Fer un PUSH (Pujar els teus canvis al server/Projecte)
        1- Canviar la vista de la app a Project
        2- Clicar amb la dreta al nom del Projecte(AppAdopcions) i clicar Git > Add
        3- Fer el mateix però clicar Git > Commit Directory
        4- Escriure el missatge i clicar Commit and Push